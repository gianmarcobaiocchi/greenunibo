import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import {
    ViroImage, 
    ViroARImageMarker,
    ViroNode,
    ViroText,
    ViroFlexView,
    ViroAnimations,
    Viro3DObject,
    ViroAmbientLight,
    ViroDirectionalLight,
    ViroAnimatedImage,
    ViroVideo,
} from 'react-viro'

import { Actions } from 'react-native-router-flux'

import { targets } from '../components/ImagesTargets'
import serverName from '../utils/Server'

export default class ImageTrackers extends Component {
    constructor(props) {
        super(props)

        this.state = {
            goalInfos: props.goalInfos,
            infos: new Array(props.goalInfos.length),
            currentInfo: 0,
            positionOnPress: null,
            isImageTracked: false,
            showQuiz: false,
            imageTracked: null,
        }

        this._getCurrentInfo = this._getCurrentInfo.bind(this)
        this._changeInfo = this._changeInfo.bind(this)
        this._getFlexView = this._getFlexView.bind(this)
    }

    componentDidMount() {
        this.state.goalInfos.forEach(e => this._uploadInfos(e.infosFile, e.key))
    }

    render() {
        if(this.state.goalInfos) {
            return (
                this.state.goalInfos.map(e => {
                    if(this.state.imageTracked === null || this.state.imageTracked === e.key) {
                        const resources = []
                        e.model.resources.forEach(r => resources.push({uri: serverName + r}))
                        return(
                            <ViroARImageMarker 
                                key={e.key}
                                target={'' + e.key} 
                                onAnchorFound={() => this.setState({isImageTracked: true, imageTracked: e.key})}>
                                <ViroNode
                                    position={[0, 0, 0.05]}
                                    scale={[0, 0, 0]}
                                    animation={{name:'popOut', run: this.state.isImageTracked}}>                            
                                    {this._getFlexView(e)}
                                </ViroNode>
                                <ViroNode
                                    position={[0, 0, 0]}
                                    renderingOrder={-1}>
                                    <ViroAmbientLight
                                        color="#fff" />
                                    <ViroDirectionalLight
                                        color="#fff"
                                        direction={[-1, -1, 0]} />
                                    <Viro3DObject
                                        source={{uri: serverName + e.model.source}}
                                        resources={resources}
                                        position={e.model.position}
                                        scale={e.model.scale}
                                        rotation={e.model.rotation}
                                        animation={{name:'',
                                            run:true,
                                            loop:true,}}
                                        ignoreEventHandling={true}
                                        type='VRX' />
                                </ViroNode>
                            </ViroARImageMarker>
                        )
                    } else {
                        return null
                    }
                })
            )            
        } else {
            return null
        }
    }

    /**
     * Gets the current info.
     */
    _getCurrentInfo(key) {
        const index = key - 1
        if(this.state.infos[index] && this.state.infos[index] !== null) {
            return this.state.infos[index][this.state.currentInfo]
        }
        return null
    }

    /**
     * Changes the displayed information.
     */
    _changeInfo(state, position, source, key) {
        if(state == 1) {
            //Click down
            this.setState({positionOnPress: position})
        } else if(state == 2) {
            //Click up
            if(this.state.positionOnPress !== null) {
                const diffX = this.state.positionOnPress[0] - position[0]
                const diffY = this.state.positionOnPress[1] - position[1]
                //Control that is a right or left swipe
                if(Math.abs(diffX) > Math.abs(diffY)) {
                    if(diffX > 0) {
                        if(this.state.currentInfo < this.state.infos[key - 1].length - 1) {
                            const nextInfo = this.state.currentInfo + 1
                            this.setState({
                                currentInfo: nextInfo,
                            })
                        } else {
                            this.setState({
                                showQuiz: true,
                            })
                        }
                    } else if(diffX < 0 && this.state.currentInfo > 0) {
                        const prevInfo = this.state.currentInfo - 1
                        this.setState({
                            currentInfo: prevInfo,
                        })
                    }
                }
                this.setState({positionOnPress: null})
            }
        }
    }

    /**
     * Uploads informations in a list.
     * The sentence splitter is ';', so that infos can be on more lines.
     */
    _uploadInfos(infosFile, key) {
        let tmp = new Array()
        tmp.push({
            animatedImage: 'swipe.gif',
            width: 0.1,
            height: 0.1,
        })
        tmp = tmp.concat(tmp, infosFile.infos)
        let infos = this.state.infos
        infos[key - 1] = tmp
        this.setState({
            infos: infos,
        })
    }

    /**
     * Gets the current flex view: informations or quiz.
     */
    _getFlexView(element) {
        if(this.state.showQuiz) {
            //Shows quiz
            return(
                <ViroFlexView
                    rotation={[-90, 0, 0]}
                    width={0.25}
                    height={0.025}
                    onClickState={(state, position, source) => 
                        this._changeInfo(state, position, source, element.key)}
                    style={infoStyle.card}>
                    <ViroFlexView
                        height={0.05}
                        width={0.1}
                        onClick={() => Actions.quiz({quiz: element.quiz, icon: element.icon, goalId: element.key})}
                        backgroundColor={'#0600FF'}>
                        <ViroText
                            height={0.05}
                            textLineBreakMode='None'
                            textClipMode='None'
                            textAlign='center'
                            textAlignVertical='center'
                            scale={[0.05, 0.05, 0.05]}
                            text='Quiz'
                            style={infoStyle.text} />
                    </ViroFlexView>
                </ViroFlexView>
                )
        } else {
            const info = this._getCurrentInfo(element.key)
            if(info !== null) {
                //Shows informations
                if(typeof info.text !== 'undefined') {
                    return(
                        <ViroFlexView
                            rotation={[-90, 0, 0]}
                            width={0.25}
                            height={0.1}
                            onClickState={(state, position, source) => 
                                this._changeInfo(state, position, source, element.key)}
                            style={infoStyle.card}>
                            <ViroText
                                height={0.1}
                                textLineBreakMode='None'
                                textClipMode='None'
                                textAlign='center'
                                textAlignVertical='top'
                                scale={[0.05, 0.05, 0.05]}
                                text={info.text}
                                style={infoStyle.text} />
                        </ViroFlexView>
                    )
                } else if(typeof info.image !== 'undefined') {
                    return(
                        <ViroFlexView
                            position={[0, 0, 0.025]}
                            rotation={[-90, 0, 0]}
                            width={0.1}
                            height={0.1}
                            onClickState={(state, position, source) => 
                                this._changeInfo(state, position, source, element.key)}
                            style={infoStyle.card}>
                            <ViroImage
                                width={info.width}
                                height={info.height}
                                source={{uri: serverName + '/res/images/' + info.image}} />
                        </ViroFlexView>
                    )
                } else if(typeof info.animatedImage !== 'undefined') {
                    return(
                        <ViroFlexView
                            position={[0, 0, 0.025]}
                            rotation={[-90, 0, 0]}
                            width={0.1}
                            height={0.1}
                            onClickState={(state, position, source) => 
                                this._changeInfo(state, position, source, element.key)}
                            style={infoStyle.card}>
                            <ViroAnimatedImage
                                width={info.width}
                                height={info.height}
                                source={{uri: serverName + '/res/images/' + info.animatedImage}} />
                        </ViroFlexView>
                    )
                } else if(typeof info.video !== 'undefined') {
                    return(
                        <ViroFlexView
                            position={[0, 0, 0.025]}
                            rotation={[-90, 0, 0]}
                            width={0.1}
                            height={0.1}
                            onClickState={(state, position, source) => 
                                this._changeInfo(state, position, source, element.key)}
                            style={infoStyle.card}>
                            <ViroVideo
                                width={info.width}
                                height={info.height}
                                source={{uri: serverName + '/res/videos/' + info.video}} />
                        </ViroFlexView>
                    )   
                } else {
                    return null
                }
            }
        }
    }
}

ViroAnimations.registerAnimations({
    popOut: {
        properties: {
            scaleX: '+= 1',
            scaleY: '+= 1',
            scaleZ: '+= 1',
        },
        duration: 500,
    },
})

const infoStyle = StyleSheet.create({
    text: {
        flex: 1,
        fontFamily: 'Arial',
        fontSize: 30,
        color: '#FFFFFF',
        fontWeight: 'bold',
    },
    answer: {
        flex: 0.3,
    },
    card: {
        flexDirection: 'column',
        alignItems: 'center',
    },
    quizButton: {
        backgroundColor: '#0600FF',
    },
})