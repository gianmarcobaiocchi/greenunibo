import React, { Component } from 'react'
import { 
    Platform, 
    ToastAndroid, 
    Alert, 
} from 'react-native'
import { 
    ViroARScene,
    ViroAmbientLight,
} from 'react-viro'

import ImageTrackers from './ImageTrackers'
import serverName from '../utils/Server'

export default class ARInfos extends Component {

    constructor() {
        super()

        this.state = {
            goalInfos: null,
        }
    }

    componentDidMount() {
        this._loadGoalInfos()
        this._notifyMessage('Frame the picture')
    }

    render() {
        if(this.state.goalInfos) {
            return (
                <ViroARScene>
                    <ViroAmbientLight color={'#FFFFFF'} intensity={200} />
                    <ImageTrackers goalInfos={this.state.goalInfos} />
                </ViroARScene>
            )
        } else {
            return (
                <ViroARScene>
                    <ViroAmbientLight color={'#FFFFFF'} intensity={200} />
                </ViroARScene>
            )
        }
    }

    /**
     * Shows on display a message.
     * @param {string} msg the message to show on display.
     */
    _notifyMessage(msg) {
        if(Platform.OS === 'android') {
            ToastAndroid.show(msg, ToastAndroid.LONG)
        } else {
            Alert.alert(msg)
        }
    }

    /**
     * Requests goals informations in json format to the server.
     */
    async _loadGoalInfos() {
        fetch(serverName + '/index.php?request=goalInfos')
        .then(response => response.json())
        .then(json => {
            this.setState({
                goalInfos: json.goals,
            })
        })
        .catch(e => console.log(e))
    }
}

module.exports = ARInfos