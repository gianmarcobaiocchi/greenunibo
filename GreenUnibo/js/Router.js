import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'
import {
    Router,
    Scene,
    Actions,
} from 'react-native-router-flux'
import AsyncStorage from '@react-native-community/async-storage'

import Login from './screens/Login'
import Home from './screens/Home'
import Profile from './screens/Profile'
import Map from './screens/Map'
import SignIn from './screens/SignIn'
import EditData from './screens/EditData'
import Location from './screens/Location'
import Infos from './screens/Infos'
import ARInfosScene from './screens/ARInfosScene'
import Quiz from './screens/Quiz'

/**
 * App flux router.
 */
export default class Routes extends Component {

    constructor() {
        super()

        this.state = {
            initialLogin: true,
        }

        this._logout = this._logout.bind(this)
        
        this._getSaved() //Try to recover usere credentials
    }

    render() {
        if(this.state.initialLogin) {
            return(
                <Router>
                    <Scene key='root'>
                        <Scene key='login' component={Login} hideNavBar={true} title='Login' initial={true} isLogout={false} />
                        <Scene key='signIn' component={SignIn} hideNavBar={true} title='Sign in' />
                        <Scene key='home' component={Home} title='Home' />
                        <Scene key='map' component={Map} title='Map' direction='vertical' />
                        <Scene key='location' component={Location} title='Location' direction='vertical' />
                        <Scene key='profile' component={Profile} title='Profile' onRight={this._logout} rightButtonImage={require('./res/icons/logout.png')} />
                        <Scene key='editData' component={EditData} title='Edit data' />
                        <Scene key='infos' component={Infos} title='Infos' />
                        <Scene key='arInfos' component={ARInfosScene} title='AR Infos' />
                        <Scene key='quiz' component={Quiz} title='Quiz' left={() => null} />
                    </Scene>
                </Router>
            )
        } else {
            return(
                <Router>
                    <Scene key='root'>
                        <Scene key='login' component={Login} hideNavBar={true} title='Login' isLogout={false} />
                        <Scene key='signIn' component={SignIn} hideNavBar={true} title='Sign in' />
                        <Scene key='home' component={Home} title='Home' initial={true} />
                        <Scene key='map' component={Map} title='Map' direction='vertical' />
                        <Scene key='location' component={Location} title='Location' direction='vertical' />
                        <Scene key='profile' component={Profile} title='Profile' onRight={this._logout} rightButtonImage={require('./res/icons/logout.png')} />
                        <Scene key='editData' component={EditData} title='Edit data' />
                        <Scene key='infos' component={Infos} title='Infos' />
                        <Scene key='arInfos' component={ARInfosScene} title='AR Infos' />
                        <Scene key='quiz' component={Quiz} title='Quiz' left={() => null} />
                    </Scene>
                </Router>
            )
        }
    }

    /**
     * User logins.
     */
    _login(username, password) {
        let formData = new FormData()
        formData.append('username', username)
        formData.append('password', password)

        fetch(serverName + '/profile.php?checkUserLogin=true', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body: formData
        })
        .then(response => response.json())
        .then(json => {
            if(json.result) {
                this.setState({
                    initialLogin: false,
                })
            } else {
                this.setState({
                    error: 'Username or password wrong',
                })
            }
        })
        .catch(e => console.log(e))
    }

    /**
     * Logs out and goes to Login screen.
     */
    async _logout() {
        const profile = {
            username: '',
            password: '',
        }
        try {
            const profileJSON = JSON.stringify(profile)
            await AsyncStorage.setItem('PROFILE', profileJSON)
            Actions.reset('login')
        } catch (e) {
            console.log(e)
        }
    }

    /**
     * Gets the recorded user credential.
     * If credentials are present, it logins automatically.
     */
    async _getSaved() {
        try {
            const profile = await AsyncStorage.getItem('PROFILE')
            const username = profile == null ? '' : JSON.parse(profile).username
            const password = profile == null ? '' : JSON.parse(profile).password
            if(username !== '' && password !== '') {
                this._login(username, password)
            }
        } catch(e) {
            console.log(e)
        }
    }
}