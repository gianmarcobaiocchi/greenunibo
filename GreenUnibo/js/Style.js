import { StyleSheet } from 'react-native'

/**
 * App UI style.
 */
export default StyleSheet.create({
    
    //Home screen
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    background: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#AAF255',
        height: '100%',
    },

    homeLogoContainer: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        height: '50%',
        width: '100%',
        top: '-5%',
    },

    homeLogo: {
        width: 120,
        height: 200,
        resizeMode: 'cover',
        borderRadius: 5,
        alignSelf: 'center'
    },

    homeLogoText: {
        color: '#505050',
        fontSize: 50,
        fontWeight: 'bold',
    },
    
    button: {
        justifyContent: 'center',
        backgroundColor: 'white',
        borderRadius: 25,
        elevation: 10,
    },

    homeButton: {
        width: '40%',
        height: '80%',
        margin: 0,
    },

    text: {
        color: 'black',
        // fontWeight: 'bold',
        fontSize: 26,
        margin: 0,
        textAlign: 'center',
    },

    iconButton: {
        height: 36,
        marginLeft: '40%',
        marginRight: '40%',
        padding: 0,
        resizeMode: 'cover',
    },

    row: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        width: '100%',
        alignItems: 'center',
    },

    tabBar: {
        height: 50,
        opacity: 0.9,
        justifyContent: 'space-between',
    },

    //ARScene screen
    balloon: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '50%',
        height:'100%', 
        backgroundColor: '#FFFFFF',
        borderRadius: 25,
    },

    textBalloon: {
        textAlignVertical: 'center',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20,
        color: '#000000',
    },

    //Login and SignIn screens
    backgroundContainer: {
        flex: 1,
        width: null,
        height: null,
        justifyContent: 'center',
        alignItems: 'center',
    },

    logoContainer: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        height: '50%',
        top: -50,
    },

    logo: {
        width: 120,
        height: 200,
        resizeMode: 'cover',
        borderRadius: 5,
    },

    logoText: {
        color: '#FFFFFF',
        fontSize: 50,
        fontWeight: 'bold',
    },

    innerContainer: {
        position: 'absolute',
        top: '40%',
        flex: 1,
        width: '100%', 
        height: '70%',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },

    inputContainer: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-around',
        height: '15%',
        margin: 0,
        padding: 2,
    },

    input: {
        width: '90%',
        height: 40,
        borderRadius: 45,
        fontSize: 26,
        color: '#FFFFFF',
        paddingHorizontal: '10%',
        paddingVertical: '1%',
        marginVertical: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.35)',
        borderBottomWidth: 2,
        borderBottomColor: '#FFFFFF',
    },

    dateInput: {
        width: '90%',
        height: '50%',
        borderRadius: 45,
        paddingHorizontal: '10%',
        paddingVertical: '1%',
        backgroundColor: 'rgba(0, 0, 0, 0.35)',
        borderColor: '#FFF',
        borderBottomWidth: 2,
    },

    btnLogin: {
        width: '80%',
        borderRadius: 45,
        backgroundColor: '#2000FF',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: '40%',
        paddingBottom: 2,
    },

    textLogin: {
        color: '#FFFFFF',
        fontSize: 28,
        textAlign: 'center',
        fontWeight: 'bold',
    },

    textSignIn: {
        marginTop: '4%', 
        fontSize: 20, 
        color: '#FFFFFF',
        textDecorationLine: 'underline',
    },

    textDescription: {
        fontSize: 18,
        color: '#FFFFFF',
        alignSelf: 'baseline',
        marginLeft: '8%',
    },

    //Profile screen
    topBalloon: {
        position: 'absolute',
        top: 0,
        height: '30%',
        width: '100%',
        backgroundColor: '#AAF255',
    },

    profIcon: {
        height: '60%', 
        width: '40%',
        borderRadius: 80,
        top: 0,
        resizeMode: 'cover', 
        alignSelf: 'center',
        marginBottom: 0,
    },

    welcome: {
        textAlign: 'center',
        fontSize: 30,
        fontWeight: 'bold',
        fontFamily: 'Arial',
        color: '#505050',
    },

    card: {
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 25,
        height: '70%',
        width: '90%',
        padding: '2%',
        elevation: 10,
    },

    dataContainer: {
        top: '-40%',
        height: '30%',
        width: '90%',
    },

    titleData: {
        textAlign: 'justify',
        fontSize: 25,
        fontWeight: 'bold',
        fontFamily: 'Arial',
        color: '#505050',
        marginBottom: '1%',
    },

    data: {
        textAlign: 'justify',
        fontSize: 20,
        marginLeft: '4%',
        lineHeight: 20,
    },

    //Edit screen
    topLowerBalloon: {
        position: 'absolute',
        top: -20,
        borderRadius: 20,
        height: '12%',
        width: '100%',
        backgroundColor: '#AAF255',
    },

    //Location screen
    locationInfosContainer: {
        top: '8%',
        height: '60%',
        maxHeight: '60%',
        width: '90%',
        padding: 0,
    },

    locationImage: {
        borderRadius: 25,
        top: '5%',
        height: '30%',
        width: '90%',
        resizeMode: 'cover',
    },

    goalIcon: {
        height: 100,
        width: 100,
        resizeMode: 'contain',
    },

    //Infos screen
    unitedNationsLogo: {
        width: '90%',
        resizeMode: 'contain',
        height: '40%',
        marginTop: '10%',
    },
})