import React, { Component } from 'react'
import { View, SafeAreaView, Image, Text } from 'react-native'
import { Actions } from 'react-native-router-flux'

import styles from '../Style'
import HomeButton from '../components/HomeButton'

/**
 * App home screen.
 */
export default class Home extends Component {

    constructor() {
        super()

        this._goToProfile = this._goToProfile.bind(this)
        this._goToARInfos = this._goToARInfos.bind(this)
        this._goToInfos = this._goToInfos.bind(this)
        this._goToMap = this._goToMap.bind(this)
    }

    render() {
        return(
            <SafeAreaView style={styles.container, styles.background}>
                    <View style={styles.homeLogoContainer}>
                        <Image source={require('../res/images/remade_white.png')} style={styles.homeLogo} />
                        <Text style={styles.homeLogoText}>REMADE</Text>
                    </View>
                    <View style={styles.container, {top: '45%', height: '50%'}}>
                        <View style={styles.row}>
                            <HomeButton buttonProps={{text: 'Frame', icon: require('../res/icons/2x/book.png'), onPress: this._goToARInfos}} />
                            <HomeButton buttonProps={{text: 'Profile', icon: require('../res/icons/2x/profile.png'), onPress: this._goToProfile}} />
                        </View>
                        <View style={styles.row}>
                            <HomeButton buttonProps={{text: 'Maps', icon: require('../res/icons/2x/location.png'), onPress: this._goToMap}} />
                            <HomeButton buttonProps={{text: 'Infos', icon: require('../res/icons/info.png'), onPress: this._goToInfos}} />
                        </View>
                    </View>
            </SafeAreaView>
        )
    }

    /**
     * Goes in ARScene screen.
     */
    _goToProfile = () => {
        Actions.profile()
    }

    /**
     * Goes in ARPortal screen.
     */
    _goToARInfos = () => {
        Actions.arInfos()
    }

    /**
     * Goes in ARPhysics screen.
     */
    _goToInfos = () => {
        Actions.infos()
    }

    /**
     * Goes in map screen.
     */
    _goToMap = () => {
        Actions.map()
    }
}