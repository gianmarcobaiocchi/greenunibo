import React, { Component } from 'react'
import {
    ImageBackground,
    View,
    Text,
    Image,
    TextInput,
    TouchableOpacity,
} from 'react-native'
import { Actions } from 'react-native-router-flux'
import AsyncStorage from '@react-native-community/async-storage'

import styles from '../Style'
import serverName from '../utils/Server'
import { encryptData, generateKey, passwordKey } from '../utils/Encryption'

/**
 * User login screen.
 */
export default class Login extends Component {

    constructor() {
        super()

        this.state = {
            username: '',
            password: '',
            error: '',
        }

        
        this._trim = this._trim.bind(this)
        this._login = this._login.bind(this)
        this._register = this._register.bind(this)
    }

    render() {
        return (
            <ImageBackground source={require('../res/images/login_background.jpg')} style={styles.backgroundContainer}>
                <View style={styles.logoContainer}>
                    <Image source={require('../res/images/remade_white.png')} style={styles.logo} />
                    <Text style={styles.logoText}>REMADE</Text>
                </View>
                <View style={styles.innerContainer}>
                    <Text style={{color: '#FF0000'}}>{this.state.error}</Text>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.input}
                            placeholder={'Username'}
                            placeholderTextColor={'#FFF'}
                            onChangeText={(text) => this.setState({username: this._trim(text)})}
                            value={this.state.username}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.input}
                            placeholder={'Password'}
                            secureTextEntry={true}
                            placeholderTextColor={'#FFF'}
                            onChangeText={(text) => this.setState({password: this._trim(text)})}
                            value={this.state.password} />
                    </View>
                    <TouchableOpacity  onPress={this._login} style={styles.btnLogin}>
                        <Text style={styles.textLogin}>Login</Text>
                    </TouchableOpacity>
                    <Text style={styles.textSignIn} onPress={() => Actions.signIn()}>
                        Sign in
                    </Text>
                </View>
            </ImageBackground>
        )
    }

    /**
     * Removes not meaningful chars from a string.
     * @param {string} text Text from which will be removed not meaningful chars.
     */
    _trim(text) {
        return text.replace( /\s\s+/g, ' ' ).trim()
    }

    /**
     * User logins.
     */
    async _login() {
        if(this.state.username === "" || this.state.password === "") {
            this.setState({
                error: 'Username and password required',
            })
        } else {
            generateKey(passwordKey, 'salt', 5000, 256)
            .then(key => {
                encryptData(this.state.password, key)
                .then(({cipher, iv}) => {
                    let formData = new FormData()
                    formData.append('username', this.state.username)
                    formData.append('password', cipher)
                    fetch(serverName + '/profile.php?checkUserLogin=true', {
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data'
                        },
                        body: formData
                    })
                    .then(response => response.json())
                    .then(json => {
                        if(json.result) {
                            this.setState({
                                error: '',
                            })
                            const profile = {
                                username: this.state.username,
                                password: cipher,
                            }
                            this._register(profile)
                            Actions.reset('home')
                        } else {
                            this.setState({
                                error: 'Username or password wrong',
                            })
                        }
                    })
                    .catch(e => this.setState({
                        error: 'Connection error',
                    }))
                })
                .catch(e => console.warn(e))
            })
            .catch(e => console.warn(e))
        }
    }

    /**
     * Records on Async Storage the user credentials.
     * @param {json} profile user credentials (username and password).
     */
    async _register(profile) {
        try {
            const profileJSON = JSON.stringify(profile)
            await AsyncStorage.setItem('PROFILE', profileJSON)
        } catch (e) {
            console.log(e)
        }
    }
}