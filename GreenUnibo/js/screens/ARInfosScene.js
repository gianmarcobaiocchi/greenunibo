import React, { Component } from 'react'
import { ViroARSceneNavigator } from 'react-viro'
import { View } from 'react-native'

/**
 * AR informations scene navigator.
 */
export default class ARInfosScene extends Component {

    constructor() {
        super()
    }

    render() {
        return(
            <View style={{flex: 1}}>
                <ViroARSceneNavigator initialScene={{scene: require('../arComponents/ARInfos')}} />
            </View>
        )
    }
}