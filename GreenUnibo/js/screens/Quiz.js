import React, { Component } from 'react'
import {
    Text, 
    View, 
    TouchableOpacity,
    SafeAreaView,
    Image,
    StyleSheet,
} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import { Actions } from 'react-native-router-flux'

import styles from '../Style'
import serverName from '../utils/Server'

export default class Quiz extends Component {
    
    constructor(props) {
        super(props)

        this.state = {
            quiz: props.quiz,
            answersClick: props.quiz.answers.map(answer => false),
            isCorrect: false,
            hasAnswered: false,
            username: '',
        }

        this._getAnswers = this._getAnswers.bind(this)
        this._getResult = this._getResult.bind(this)

        this._getUsername()
    }
    
    render() {
        if(this.state.quiz) {
            return (
                <SafeAreaView style={[styles.container, styles.background, {backgroundColor: '#FFF'}]}>
                    <View style={styles.topLowerBalloon}></View>
                    <Image
                        source={{uri: serverName + '/res/images/' + this.props.icon}} 
                        style={[styles.profIcon, quizStyle.topIcon]} />
                    <View style={[styles.innerContainer, quizStyle.innerContainer]}>
                        <Text style={[styles.text, quizStyle.question]}>{this.state.quiz.question}</Text>
                        {this._getAnswers()}
                        {this._getResult()}
                        <Text style={[styles.textSignIn, quizStyle.textHome]} onPress={() => Actions.popTo('home')}>
                            Home
                        </Text>
                    </View>
                </SafeAreaView>
            )
        } else {
            return(
                <SafeAreaView style={[styles.container, styles.background, {backgroundColor: '#FFF'}]}>
                    <Text>Caricamento...</Text>
                </SafeAreaView>
            )
        }
    }

    /**
     * Gets all the question's answers.
     */
    _getAnswers() {
        return this.state.quiz.answers.map(answer => {
            const size = answer.text.length < 15 ? 26 : 14
            const onClickColor = answer.correct ? 'rgba(0, 255, 0, 0.5)' : 'rgba(255, 0, 0, 0.5)'
            if(this.state.answersClick[answer.key]) {
                return(
                    <TouchableOpacity 
                        key={answer.key}
                        style={[styles.input, quizStyle.input, {backgroundColor: onClickColor}]}>
                        <Text style={[styles.text, {fontWeight: 'bold', fontSize: size}]}>{answer.text}</Text>
                    </TouchableOpacity>)
            } else {
                return(
                    <TouchableOpacity
                        key={answer.key}
                        style={[styles.input, quizStyle.input]}
                        onPress={() => {
                            //Check if any other answer has been clicked
                            if(this.state.answersClick.filter(a => a===true).length === 0) {
                                this._answerClick(answer.key)
                            }
                        }}>
                        <Text style={[styles.text, {fontSize: size}]}>{answer.text}</Text>
                    </TouchableOpacity>)
            }
        })
    }
    
    /**
     * Does the onClick function of the answers.
     * @param {int} key answer's identifier.
     */
    _answerClick(key) {
        tmp = this.state.answersClick
        tmp[key] = true
        const isCorrect = this.state.quiz.answers[key].correct
        this.setState({
            answersClick: tmp,
            hasAnswered: true,
            isCorrect: isCorrect,
        })
        //Adding points to the score
        if(isCorrect) {
            let formData = new FormData()
            formData.append('username', this.state.username)
            formData.append('points', '5')
            formData.append('goalId', this.props.goalId)
            fetch(serverName + '/profile.php?addPoints=true', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data'
                },
                body: formData
            })
            .then()
            .catch(e => console.log(e))
        }
    }

    /**
     * Returns the result view component.
     */
    _getResult() {
        if(this.state.hasAnswered) {
            if(this.state.isCorrect) {
                return(
                    <View style={{height: '20%'}}>
                        <Text style={styles.text}>
                            Corretto :)
                        </Text>
                    </View>
                )
            } else {
                return(
                    <View style={{height: '20%'}}>
                        <Text style={styles.text}>
                            Sbagliato :(
                        </Text>
                    </View>
                )
            }
        }
        return(<View style={{height: '20%'}}></View>)
    }

    /**
     * Fetches the username from Async Storage.
     */
    async _getUsername() {
        try {
            const profile = await AsyncStorage.getItem("PROFILE")
            const username = profile == null ? '' : JSON.parse(profile).username
            this.setState({username: username})
        } catch(e) {
            console.log(e)
        }
    }
}

const quizStyle = StyleSheet.create({
    topIcon: {
        position: 'absolute', 
        top: '2%', 
        height: '10%', 
        width: '20%',
        borderRadius: 5,
    },
    input: {
        color: '#000',
        backgroundColor: 'rgba(0, 0, 0, 0.15)',
        borderBottomColor: '#00000000',
    },
    innerContainer: {
        top: '20%',
        height: '70%',
        justifyContent: 'space-between',
    },
    textHome: {
        color: '#505050',
    },
    question: {
        fontSize: 20, 
        marginBottom: '5%',
        color: '#000',
        fontWeight: 'bold',
    },
})