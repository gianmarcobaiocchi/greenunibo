import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    SafeAreaView,
} from 'react-native'
import { Actions } from 'react-native-router-flux'
import DatePicker from 'react-native-datepicker'
import AsyncStorage from '@react-native-community/async-storage'

import styles from '../Style'
import serverName from '../utils/Server'
import { encryptData, generateKey, passwordKey } from '../utils/Encryption'

/**
 * User sign in screen.
 */
export default class EditData extends Component {

    constructor() {
        super()

        this.state = {
            profile: {
                username: '',
                name: '',
                surname: '',
                password: '',
                birthday: new Date(),
            },
            fetchedData: false,
        }

        this._trim = this._trim.bind(this)
        this._submit = this._submit.bind(this)
        this._getInputs = this._getInputs.bind(this)

        this._getUserData()
    }

    render() {
        return (
            <SafeAreaView style={[styles.container, styles.background, {backgroundColor: '#FFF'}]}>
                <View style={styles.topLowerBalloon}></View>
                <Image source={require('../res/icons/profile-icon.png')} style={[styles.profIcon, {position: 'absolute', top: '2%', height: '10%', width: '20%'}]} />
                {this._getInputs()}
            </SafeAreaView>
        )
    }

    /**
     * Removes not meaningful chars from a string.
     * @param {string} text Text from which will be removed not meaningful chars.
     */
    _trim(text) {
        return text.replace( /\s\s+/g, ' ' ).trim()
    }

    /**
     * Gets user credentials from Async Storage.
     */
    _getUserData = async () => {
        let formData = new FormData()
        try {
            const profile = await AsyncStorage.getItem('PROFILE')
            const username = profile == null ? '' : JSON.parse(profile).username
            this.setState({username: username})
            formData.append('username', '' + username)
            fetch(serverName + '/profile.php?getUserData=true', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data'
                },
                body: formData
            })
            .then(response => response.json())
            .then(json => {
                if(json.user !== null) {
                    this.setState({
                        profile: {
                            username: json.user.username,
                            name: json.user.first_name,
                            surname: json.user.second_name,
                            password: "",
                            birthday: new Date(json.user.birthday),
                        },
                        fetchedData: true,
                    })
                }
            })
            .catch(e => console.log('Utente non trovato!'))
        } catch(e) {
            console.log(e)
        }
    }

    /**
     * Records all user data.
     * @param {json} profile user data (username, password, name, etc.).
     */
    _submit = async (profile) => {
        generateKey(passwordKey, 'salt', 5000, 256)
        .then(key => {
            encryptData(profile.password, key)
            .then(({cipher, iv}) => {
                const formData = new FormData()
                formData.append('username', profile.username)
                formData.append('password', cipher)
                formData.append('firstName', profile.name)
                formData.append('secondName', profile.surname)
                const date = new Date(profile.birthday)
                const month = date.getMonth() + 1
                formData.append('birthday', date.getFullYear() + '-' + month + '-' + date.getDate())
                fetch(serverName + '/profile.php?editUserData=true', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data'
                    },
                    body: formData
                })
                .then(response => response.json())
                .then(json => {
                    if(json.result) {
                        Actions.popTo('home')
                    }
                })
                .catch(e => console.log(e))
            })
            .catch(e => console.log(e))
        })
    }
    
    /**
     * Returns inputs components only if user data are fetched.
     */
    _getInputs() {
        if(this.state.fetchedData) {
            return(
                <View style={[styles.innerContainer, {height: '90%', top: '10%'}]}>
                    <View style={[styles.inputContainer, modifyStyles.inputContainer]}>
                        <Text style={[styles.textDescription, modifyStyles.textDescription]}>First name</Text>
                        <TextInput
                            style={[styles.input, modifyStyles.input]}
                            placeholder={'First name'}
                            placeholderTextColor={'#000'}
                            defaultValue={this.state.profile.name}
                            underlineColorAndroid='transparent'
                            onChangeText={(text) => this.setState({profile: {...this.state.profile, name: this._trim(text)}})} />
                    </View>
                    <View style={[styles.inputContainer, modifyStyles.inputContainer]}>
                        <Text style={[styles.textDescription, modifyStyles.textDescription]}>Second name</Text>
                        <TextInput
                            style={[styles.input, modifyStyles.input]}
                            placeholder={'Second name'}
                            placeholderTextColor={'#000'}
                            defaultValue={this.state.profile.surname}
                            underlineColorAndroid='transparent'
                            onChangeText={(text) => this.setState({profile: {...this.state.profile, surname: this._trim(text)}})} />
                    </View>
                    <View style={[styles.inputContainer, modifyStyles.inputContainer]}>
                        <Text style={[styles.textDescription, modifyStyles.textDescription]}>Username</Text>
                        <TextInput
                            style={[styles.input, modifyStyles.input, {color: 'rgba(0, 0, 0, 0.5)'}]}
                            placeholder={'Username'}
                            placeholderTextColor={'#000'}
                            defaultValue={this.state.profile.username}
                            underlineColorAndroid='transparent'
                            editable={false}
                            onChangeText={(text) => this.setState({profile: {...this.state.profile, username: this._trim(text)}})} />
                    </View>
                    <View style={[styles.inputContainer, modifyStyles.inputContainer]}>
                        <Text style={[styles.textDescription, modifyStyles.textDescription]}>Password</Text>
                        <TextInput
                            style={[styles.input, modifyStyles.input]}
                            placeholder={'Password'}
                            secureTextEntry={true}
                            placeholderTextColor={'#000'}
                            defaultValue={this.state.profile.password}
                            underlineColorAndroid='transparent'
                            onChangeText={(text) => this.setState({profile: {...this.state.profile, password: this._trim(text)}})} />
                    </View>
                    <View style={[styles.inputContainer, modifyStyles.inputContainer]}>
                        <Text style={[styles.textDescription, modifyStyles.textDescription]}>Birthday</Text>
                        <DatePicker
                            customStyles={{
                                dateInput: {
                                    alignItems: 'baseline', 
                                    borderWidth: 0,
                                },
                                dateText: {
                                    fontSize: 26,
                                    color: '#000',
                                },
                            }}
                            style={[styles.dateInput, modifyStyles.dateInput]}
                            date={this.state.profile.birthday}
                            mode='date'
                            placeholder='Select date'
                            format='YYYY-MM-DD'
                            confirmBtnText='Confirm'
                            cancelBtnText='Cancel'
                            onDateChange={(date) => this.setState({profile: {...this.state.profile, birthday: new Date(date)}})} />
                    </View>
                    <TouchableOpacity
                        onPress={() => this._submit(this.state.profile)}
                        style={[styles.btnLogin, modifyStyles.btnSignIn]}>
                        <Text style={styles.textLogin}>Submit</Text>
                    </TouchableOpacity>
                </View>
            )
        }
        return null
    }
}

/**
 * Specific EditData screen layout settings.
 */
const modifyStyles = StyleSheet.create({
    inputContainer: {
        height: '15%',
    },

    btnSignIn: {
        height: '6%',
        marginTop: '15%',
    },

    textDescription: {
        color: '#000',
    },

    input: {
        color: '#000',
        backgroundColor: 'rgba(0, 0, 0, 0.15)',
        borderBottomColor: '#000',
    },

    dateInput: {
        backgroundColor: 'rgba(0, 0, 0, 0.15)',
        borderBottomColor: '#000',
    },
})