import React, { Component } from 'react'
import {
    ImageBackground,
    View,
    Text,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
} from 'react-native'
import { Actions } from 'react-native-router-flux'
import DatePicker from 'react-native-datepicker'
import AsyncStorage from '@react-native-community/async-storage'

import styles from '../Style'
import serverName from '../utils/Server'
import { encryptData, generateKey, passwordKey } from '../utils/Encryption'


/**
 * User sign in screen.
 */
export default class SignIn extends Component {

    constructor() {
        super()

        this.state = {
            profile: {
                username: '',
                name: '',
                surname: '',
                birthday: new Date(), //Today
                password: '',
            },
            realm: null,
        }

        this._trim = this._trim.bind(this)
        this._signIn = this._signIn.bind(this)
    }

    render() {
        return (
            <ImageBackground source={require('../res/images/login_background.jpg')} style={styles.backgroundContainer}>
                <View style={[styles.logoContainer, {height: '30%'}]}>
                    <Text style={styles.logoText}>REMADE</Text>
                </View>
                <View style={[styles.innerContainer, {height: '88%', top: '12%'}]}>
                    <View style={[styles.inputContainer, signInStyles.inputContainer]}>
                        <Text style={styles.textDescription}>First name</Text>
                        <TextInput
                            style={styles.input}
                            placeholder={'First name'}
                            placeholderTextColor={'#FFF'}
                            underlineColorAndroid='transparent'
                            onChangeText={(text) => this.setState({profile: {...this.state.profile, name: this._trim(text)}})} />
                    </View>
                    <View style={[styles.inputContainer, signInStyles.inputContainer]}>
                        <Text style={styles.textDescription}>Second name</Text>
                        <TextInput
                            style={styles.input}
                            placeholder={'Second name'}
                            placeholderTextColor={'#FFF'}
                            underlineColorAndroid='transparent'
                            onChangeText={(text) => this.setState({profile: {...this.state.profile, surname: this._trim(text)}})} />
                    </View>
                    <View style={[styles.inputContainer, signInStyles.inputContainer]}>
                        <Text style={styles.textDescription}>Username</Text>
                        <TextInput
                            style={styles.input}
                            placeholder={'Username'}
                            placeholderTextColor={'#FFF'}
                            underlineColorAndroid='transparent'
                            onChangeText={(text) => this.setState({profile: {...this.state.profile, username: this._trim(text)}})} />
                    </View>
                    <View style={[styles.inputContainer, signInStyles.inputContainer]}>
                        <Text style={styles.textDescription}>Password</Text>
                        <TextInput
                            style={styles.input}
                            placeholder={'Password'}
                            secureTextEntry={true}
                            placeholderTextColor={'#FFF'}
                            underlineColorAndroid='transparent'
                            onChangeText={(text) => this.setState({profile: {...this.state.profile, password: this._trim(text)}})} />
                    </View>
                    <View style={[styles.inputContainer, signInStyles.inputContainer]}>
                        <Text style={styles.textDescription}>Birthday</Text>
                        <DatePicker
                            customStyles={{
                                dateInput: {
                                    alignItems: 'baseline', 
                                    borderWidth: 0,
                                },
                                dateText: {
                                    fontSize: 26,
                                    color: '#FFFFFF',
                                },
                            }}
                            style={styles.dateInput}
                            date={this.state.profile.birthday}
                            mode='date'
                            placeholder='Select date'
                            format='YYYY-MM-DD'
                            confirmBtnText='Confirm'
                            cancelBtnText='Cancel'
                            onDateChange={(date) => this.setState({profile: {...this.state.profile, birthday: new Date(date)}})} />
                    </View>
                    <TouchableOpacity 
                        onPress={() => this._signIn(this.state.profile)}
                        style={[styles.btnLogin, signInStyles.btnSignIn]}>
                        <Text style={styles.textLogin}>Sign in</Text>
                    </TouchableOpacity>
                    <Text style={styles.textSignIn} onPress={() => Actions.pop()}>
                        Login
                    </Text>
                </View>
            </ImageBackground>
        )
    }

    /**
     * Removes not meaningful chars from a string.
     * @param {string} text Text from which will be removed not meaningful chars.
     */
    _trim(text) {
        return text.replace( /\s\s+/g, ' ' ).trim()
    }

    /**
     * Records all user data.
     * @param {json} profile user data (username, password, name, etc.).
     */
    _signIn = async (profile) => {
        generateKey(passwordKey, 'salt', 5000, 256)
        .then(key => {
            encryptData(profile.password, key)
            .then(({cipher, iv}) => {
                const formData = new FormData()
                formData.append('username', profile.username)
                formData.append('password', cipher)
                formData.append('firstName', profile.name)
                formData.append('secondName', profile.surname)
                const date = new Date(profile.birthday)
                const month = date.getMonth() + 1
                formData.append('birthday', date.getFullYear() + '-' + month + '-' + date.getDate())
                fetch(serverName + '/profile.php?registerUser=true', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data'
                    },
                    body: formData
                })
                .then(response => response.json())
                .then(json => {
                    if(json.result) {
                        const profileJSON = JSON.stringify({
                            username: this.state.profile.username, 
                            password: cipher,
                        })
                        AsyncStorage.setItem('PROFILE', profileJSON)
                        Actions.reset('home')
                    }
                })
                .catch(e => console.log(e))
            })
            .catch(e => console.log(e))
        })
    }
}

const signInStyles = StyleSheet.create({
    inputContainer: {
        height: '15%',
    },

    btnSignIn: {
        height: '6%',
        marginTop: '10%',
    },
})