import React, { Component } from 'react'
import { 
    View, 
    StyleSheet, 
    Platform,
} from 'react-native'
import MapboxGL from '@react-native-mapbox-gl/maps'
import GeoLocation from '@react-native-community/geolocation'

import ShapeSource from '../components/ShapeSource'
import serverName from '../utils/Server'

//Mapbox access token
MapboxGL.setAccessToken("pk.eyJ1IjoiZ2lhbm1hcmNvYmFpb2NjaGkiLCJhIjoiY2ticml5dHZkMnc5ZTJ6cXZqczVmajR5bSJ9.SOai_LPFXPc0kSM-bOwhtw")

/**
 * Map screen.
 */
export default class Map extends Component {

    constructor() {
        super()

        this.state = {
            isAndroidPermissionGranted: false,
            isFetchingAndroidPermission: Platform.OS === 'android',
            coordinates: [[12.6456, 44.006]],
            showUserLocation: true,
            location: [12.6456, 44.006],
            userLocationAvailable: false,
            markers: null,
        }

        this._getUserPosition = this._getUserPosition.bind(this)
        this._getShapeSource = this._getShapeSource.bind(this)
    }

    async componentWillMount() {
        if(Platform.OS === 'android') {
            const isGranted = await MapboxGL.requestAndroidLocationPermissions();
            this.setState({
                isAndroidPermissionGranted: isGranted,
                isFetchingAndroidPermission: false,
            })
        }
    }

    componentDidMount() {
        this._uploadMarkers()
    }

    render() {
        return (
        <View style={mapStyle.page}>
            <View style={mapStyle.container}>
                <MapboxGL.MapView 
                    style={mapStyle.map}
                    ref={c=>(this._map=c)}
                    zoomLevel={14}
                    centerCoordinates={this.state.coordinates[0]}
                    showUserLocation={this.state.showUserLocation}
                    >
                    <MapboxGL.Camera
                        defaultSettings={{
                            zoomLevel: 16,
                            centerCoordinate: this.state.location,
                        }}
                        ref={c=>(this.camera=c)}
                        animationMode={'flyTo'}
                        animationDuration={0}
                        followUserLocation
                        followUserMode={'normal'} />
                    {this._getUserPosition()}
                    {this._getShapeSource()}
                </MapboxGL.MapView>
            </View>
        </View>
        )
    }

    /**
     * Gets the user position component.
     */
    _getUserPosition() {
        this._checkUserPositionAvailable()
        if((Platform.OS === 'android' && !this.state.isAndroidPermissionGranted) || !this.state.userLocationAvailable) {
            return null
        } else {
            return(<MapboxGL.UserLocation visible={true} /*onUpdate={this._updateTracking}*/ />)
        }
    }

    /**
     * Checks if user position is available.
     */
    _checkUserPositionAvailable() {
        GeoLocation.getCurrentPosition(
            position => {
                this.setState({userLocationAvailable : position !== null})
            },
            e => {
                console.log(e)
                this.setState({userLocationAvailable : false})
            })
    }

    /**
     * Returns all map markers if setted.
     */
    _getShapeSource() {
        if(this.state.markers) {
            return(
                <ShapeSource markers={this.state.markers} />
            )
        }
        return null;
    }

    /**
     * Uploads from server all map markers infos.
     */
    async _uploadMarkers() {
        fetch(serverName + '/locations.php?request=locations')
        .then(response => response.json())
        .then(json => {
            this.setState({
                markers: json,
            })
        })
        .catch(e => console.log(e))
    }
}

/**
 * Map UI style.
 */
const mapStyle = StyleSheet.create({
  page: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
  },

  container: {
    height: '100%',
    width: '100%',
    backgroundColor: "tomato",
  },

  map: {
    flex: 1
  },
});