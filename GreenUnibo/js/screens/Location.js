import React, { Component } from 'react'
import { 
    View, 
    Text, 
    SafeAreaView, 
    Image, 
    StyleSheet,
    ScrollView,
} from 'react-native'

import styles from '../Style'
import serverName from '../utils/Server'

/**
 * Location informations screen.
 */
export default class Location extends Component {

    constructor(props) {
        super(props)

        this.state = {
            coordinates: props.coordinates,
            name: props.place.properties.place.name,
        }

        this._getLocationName()
    }

    render() {
        return (
            <SafeAreaView style={[styles.container, styles.background, {backgroundColor: '#FFF'}]}>
                <View style={{width: '100%', height: '100%', alignItems: 'center'}}>
                    <View style={styles.topBalloon}></View>
                    <Image source={{uri: serverName + '/res/images/' + this.props.place.properties.place.image}} style={styles.locationImage} />
                    <ScrollView style={styles.locationInfosContainer}>
                        <Text style={[styles.data, locationStyle.data]}>
                            {this.state.name}
                        </Text>
                        <Text style={[styles.titleData, locationStyle.titleData]}>
                            Description
                        </Text>
                        <Text style={[styles.data, locationStyle.data]}>
                            {this.props.place.properties.place.description}
                        </Text>
                        <Text style={[styles.titleData, locationStyle.titleData]}>
                            {this.props.place.properties.goal.name}
                        </Text>
                        <View style={{alignItems: 'center'}}>
                            <Image source={{uri: serverName + '/res/images/' + this.props.place.properties.goal.icon}} style={styles.goalIcon} />
                        </View>
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }

    /**
     * Gets location name from OSM Nominatim service.
     */
    async _getLocationName() {
        fetch('https://nominatim.openstreetmap.org/reverse?&format=json&lat=' + this.state.coordinates[1] + '&lon=' + this.state.coordinates[0])
        .then(response => response.json())
        .then(json => {
            if(json.display_name) {
                this.setState({name: json.display_name})
            } else {
                this.setState({name: 'Location not found'})
            }
        })
        .catch(e => console.log(e))
    }
}

const locationStyle = StyleSheet.create({
    data: {
        marginLeft: 0,
    },
    titleData: {
        marginTop: '5%',
    }
})