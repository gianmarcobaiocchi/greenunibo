import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    SafeAreaView,
    TouchableOpacity,
    ScrollView,
} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import { Actions } from 'react-native-router-flux'

import styles from '../Style'
import serverName from '../utils/Server'

/**
 * User profile screen.
 */
export default class Profile extends Component {

    constructor() {
        super()
        
        this.state = {
            username: '',
            profile: null,
            rankingPosition: null,
            visitedGoals: null,
        }

        this._getUserData = this._getUserData.bind(this)
        this._getWelcome = this._getWelcome.bind(this)
        this._getVisitedGoals = this._getVisitedGoals.bind(this)

        this._getUsername()
    }
    
    render() {
        return (
            <SafeAreaView style={[styles.container, styles.background, {backgroundColor: '#FFF'}]}>
                <View style={[styles.row, {flexDirection: 'column'}]}>
                    <View style={styles.topBalloon}></View>
                        <View style={[styles.row, {flex: 0}]}>
                            <View style={styles.card}>
                                <Image source={require('../res/icons/profile-icon.png')} style={styles.profIcon} /> 
                                {this._getWelcome()}
                            </View>
                        </View>
                        <ScrollView style={{flex: 1, width: '100%'}}>
                            <View style={{width: '100%', height: '100%', alignContent: 'space-around', justifyContent: 'space-around', padding: '2%'}}>
                                {this._getUserData()}
                                {this._getVisitedGoals()}
                                <TouchableOpacity  onPress={() => Actions.editData()} style={[styles.btnLogin, {alignSelf: 'center', marginTop: '10%'}]}>
                                    <Text style={styles.textLogin}>Edit data</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
            </SafeAreaView>
        )
    }

    /**
     * Gets user credentials from Async Storage.
     */
    _getUsername = async () => {
        let formData = new FormData()
        try {
            const profile = await AsyncStorage.getItem('PROFILE')
            const username = profile == null ? '' : JSON.parse(profile).username
            this.setState({username: username})
            formData.append('username', '' + username)
            fetch(serverName + '/profile.php?getUserData=true', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data'
                },
                body: formData
            })
            .then(response => response.json())
            .then(json => {
                if(json.user !== null) {
                    this.setState({
                        profile: json.user,
                        rankingPosition: json.rankingPosition,
                        visitedGoals: json.visitedGoals,
                    })
                }
            })
            .catch(e => console.log(e))
        } catch(e) {
            console.log(e)
        }
    }

    /**
     * Gets user data components.
     */
    _getUserData() {
        if(this.state.profile) {
            return(
                <View style={[styles.row, {flexDirection: 'column'}]}>
                    <View style={styles.dataContainer}>
                        <Text style={styles.titleData}>
                            Personal data
                        </Text>
                        <Text style={styles.data}>
                            Username: {this.state.username}{'\n'}
                            First name: {this.state.profile.first_name}{'\n'}
                            Second name: {this.state.profile.second_name}{'\n'}
                            Birthday: {this._dateToString(this.state.profile.birthday)}{'\n'}
                        </Text>
                        <Text style={styles.titleData}>
                            Ranking position
                        </Text>
                        <View style={{alignItems: 'center'}}>
                            <View style={{flexDirection: 'row', alignItems: 'flex-start'}}>
                                <Text style={[styles.data, {marginTop: '4%'}]}>{this.state.rankingPosition}°</Text>
                                <Image source={require('../res/icons/leaderboard.png')} style={{marginLeft: '2%'}} />
                            </View>
                            <Text style={[styles.data, {marginTop: '5%'}]}>
                                {this.state.profile.score} pts
                            </Text>
                        </View>
                    </View>
                </View>
            )
        } else {
            return(<Text style={{color: '#FF0000'}}>Caricamento...</Text>)
        }
    }

    /**
     * Gets all visited goals graphic components.
     */
    _getVisitedGoals() {
        if(this.state.visitedGoals && this.state.visitedGoals.length !== 0) {
            return (
                <View style={{flex: 1, flexDirection: 'column'}}>
                    <Text style={[styles.titleData, {marginLeft: '5%'}]}>
                        Visited goals
                    </Text>
                    {this.state.visitedGoals.map(g => {
                        const currentIndex = this.state.visitedGoals.indexOf(g)
                        if(currentIndex % 2 == 0) {
                            if(currentIndex < this.state.visitedGoals.length - 1) {
                                const nextElement = this.state.visitedGoals[currentIndex + 1]
                                return(
                                    <View key={g.id} style={{flex: 1, flexDirection: 'row', marginTop: '5%'}}>
                                        <Image 
                                            key={g.id} 
                                            source={{uri: serverName + '/res/images/' + g.icon}} 
                                            style={[styles.goalIcon, {width: '50%'}]} />
                                        <Image 
                                            key={nextElement.id} 
                                            source={{uri: serverName + '/res/images/' + nextElement.icon}} 
                                            style={[styles.goalIcon, {width: '50%'}]} />
                                    </View>
                                )
                            } else {
                                return(
                                    <View key={g.id} style={{flex: 1, flexDirection: 'row', marginTop: '5%'}}>
                                        <Image 
                                            key={g.id} 
                                            source={{uri: serverName + '/res/images/' + g.icon}} 
                                            style={[styles.goalIcon, {width: '50%'}]} />
                                    </View>
                                )
                            }
                        }
                        return null
                    })}
                </View>
            )
        } else {
            return null
        }
    }

    /**
     * Returns the date in string format.
     * @param {Date} date the date to print.
     */
    _dateToString(date) {
        const tmp = new Date(date)
        const month = tmp.getMonth() + 1
        return tmp.getDate() + '/' + month + '/' + tmp.getFullYear()
    }

    /**
     * Returns the welcome string.
     */
    _getWelcome() {
        if(this.state.profile) {
            return (<Text style={styles.welcome}>
                        Welcome {this.state.profile.first_name}!
                    </Text>)
        }
        return (<Text style={styles.welcome}>
                    Welcome!
                </Text>)
    }
}