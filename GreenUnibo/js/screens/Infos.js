import React, { Component } from 'react'
import { 
    Text, 
    View, 
    StyleSheet,
    SafeAreaView,
    Image,
    TouchableOpacity,
    Linking, 
} from 'react-native'

import styles from '../Style'

export default class Infos extends Component {
    render() {
        return (
            <SafeAreaView style={[styles.container, styles.background, {backgroundColor: '#FFF'}]}>
                <View style={{width: '100%', height: '100%', alignItems: 'center'}}>
                    <View style={[styles.topLowerBalloon, infosStyle.topBalloon]}></View>
                    <View style={styles.homeLogoContainer}>
                        <Image source={require('../res/images/remade_white.png')} style={styles.homeLogo} />
                        <Text style={styles.homeLogoText}>REMADE</Text>
                    </View>
                    <View style={infosStyle.innerContainer}>
                        <Text style={[styles.data, {marginLeft: 0}]}>
                            REMade is a degree project thought
                            to sensitize people about the Sustainable
                            Development.
                            The Sustainable Development is an 
                            economic development that safeguards 
                            enviroment and free goods for future
                            generations. United Nations have choosen 
                            17 goals (SDGs) to ensure the correct work 
                            of the Sustainable Development.
                        </Text>
                        <View style={{alignItems: 'center'}}>
                            <Image source={require('../res/images/united_nations.png')} style={styles.unitedNationsLogo} />
                            <TouchableOpacity
                                onPress={async () => {
                                    const link = 'https://sdgs.un.org/'
                                    const supported = Linking.canOpenURL(link)
                                    if(supported) {
                                        await Linking.openURL(link)
                                    } else {
                                        Alert.alert('Link not supported :(', 'This URL is not supported, I\'m very very sorry...')
                                    }
                                }}>
                                <Text style={[styles.data, infosStyle.link]}>
                                    https://sdgs.un.org/
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const infosStyle = StyleSheet.create({
    topBalloon: {
        height: '45%',
        borderRadius: 40,
        top: -40,
    },
    innerContainer: {
        top: '42%', 
        width: '90%',
        height: '56%',
        marginTop: '2%',
    },
    link: {
        marginLeft: 0,
        alignSelf: 'center',
        fontSize: 18,
        marginTop: '5%',
        color: '#505050',
    },
})
