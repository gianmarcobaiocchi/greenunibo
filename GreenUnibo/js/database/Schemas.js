import Realm  from 'realm'

export const USERS_SCHEMA = 'Users'

/**
 * Simple user schema.
 */
export const UsersSchema = {
    name: 'Users',
    primaryKey: 'username',
    properties: {
        username: 'string', 
        password: 'string',
        name: 'string', 
        surname: 'string', 
        birthday: {type: 'date', default: new Date(1980, 1, 1)},
        score: {type: 'int', default: 0},
    }
}

const databaseOptions = {
    path: 'usersSchema.realm',
    schema: [UsersSchema],
    schemaVersion: 0,
}

/**
 * Inserts a new user in the database.
 * @param {UsersSchema} user user data to record.
 */
export const insertNewUser = (user) => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            realm.create(USERS_SCHEMA, user)
            resolve(user)
        })
    }).catch(e => reject(e))
})

/**
 * Gets a specific user with the id.
 * @param {string} username user id.
 */
export const getUser = (username) => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        const user = realm.objectForPrimaryKey(USERS_SCHEMA, username)
        // const user = realm.objects(USERS_SCHEMA).filtered('username == $0', username)
        resolve(user)
    }).catch(e => reject(e))
})

/**
 * Gets all users who are in the database.
 */
export const getAllUsers = () => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        const users = realm.objects(USERS_SCHEMA)
        resolve(users)
    }).catch(e => reject(e))
})

export const addScore = (username, points) => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        const user = realm.objectForPrimaryKey(USERS_SCHEMA, username)
        realm.write(realm => {
            user.score = user.score + points
            resolve(user)
        })
    }).catch(e => reject(e))
})

export const editUser = (userNew) => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        const user = realm.objectForPrimaryKey(USERS_SCHEMA, userNew.username)
        realm.write(realm => {
            user.name = userNew.name
            user.surname = userNew.surname
            user.birthday = userNew.birthday
            user.password = userNew.password
            resolve(user)
        })
    }).catch(e => reject(e))
})

export default new Realm(databaseOptions)