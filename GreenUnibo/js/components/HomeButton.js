import React, { Component } from 'react'
import { View, TouchableOpacity, Text, Image} from 'react-native'

import styles from '../Style'

/**
 * Simple button, that is used in home screen.
 */
export default class HomeButton extends Component {
    render() {
        return(
            <TouchableOpacity style={styles.homeButton} onPress={this.props.buttonProps.onPress}>
                <View style={[styles.button, {width: '100%', height: '100%'}]} >
                    <Image source={this.props.buttonProps.icon} style={styles.iconButton} />
                    <Text style={styles.text}>{this.props.buttonProps.text}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

module.exports = HomeButton