import React, { Component } from 'react'
import MapboxGL from '@react-native-mapbox-gl/maps'
import { Actions } from 'react-native-router-flux'

/**
 * Map marker.
 */
export default class ShapeSource extends Component {

    constructor(props) {
        super(props)

        this.state = {
            markers: this.props.markers,
            icon: {
                iconImage: require('../res/icons/red_place.png'),
                iconAllowOverlap: true,
                iconSize: 0.5,
            },
        }
        
        this._markerPress = this._markerPress.bind(this)
    }
    
    render() {
        return (
            <MapboxGL.ShapeSource
                id = {'shapeID'}
                hitbox={{width: 20, height: 20}}
                onPress={this._markerPress}
                shape={this.state.markers}>
                <MapboxGL.SymbolLayer
                    id={'locationsIcon'}
                    style={this.state.icon} />
            </MapboxGL.ShapeSource>
        )
    }

    /**
     * Opens a new screens where are displayed some informations about the marker location.
     * @param {event} e marker properties.
     */
    _markerPress(e) {
        const feature = e.nativeEvent.payload
        Actions.location({place: feature, title: feature.properties.place.name, coordinates: feature.geometry.coordinates})
    }
}
