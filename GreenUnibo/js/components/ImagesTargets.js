import { ViroARTrackingTargets } from 'react-viro'
import serverName from '../utils/Server'

export const targets = ViroARTrackingTargets.createTargets({
    '1': {
        source: {uri: serverName + '/res/targets/logo_goal_1.png'},
        orientation: 'Up',
        physicalWidth: 0.1,
    },
    '2': {
        source: {uri: serverName + '/res/targets/logo_goal_2.png'},
        orientation: 'Up',
        physicalWidth: 0.1,
    },
    '3': {
        source: {uri: serverName + '/res/targets/logo_goal_3.png'},
        orientation: 'Up',
        physicalWidth: 0.1,
    },
    '4': {
        source: {uri: serverName + '/res/targets/logo_goal_4.png'},
        orientation: 'Up',
        physicalWidth: 0.1,
    },
    '5': {
        source: {uri: serverName + '/res/targets/logo_goal_5.png'},
        orientation: 'Up',
        physicalWidth: 0.1,
    },
    '6': {
        source: {uri: serverName + '/res/targets/logo_goal_6.png'},
        orientation: 'Up',
        physicalWidth: 0.1,
    },
    '7': {
        source: {uri: serverName + '/res/targets/logo_goal_7.png'},
        orientation: 'Up',
        physicalWidth: 0.1,
    }
})