import { NativeModules, Platform } from 'react-native'
var Aes = NativeModules.Aes

const ivConst = '78f39e9a12df514a71341e5adb75e8c2'
export const passwordKey = 'Remade'

export const generateKey = (password, salt, cost, length) => Aes.pbkdf2(password, salt, cost, length)

export const encryptData = (text, key) => {
    return Aes.randomKey(16).then(iv => {
        return Aes.encrypt(text, key, ivConst).then(cipher => ({
            cipher,
            iv,
        }))
    })
}