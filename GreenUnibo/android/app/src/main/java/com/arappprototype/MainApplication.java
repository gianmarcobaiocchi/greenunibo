package com.arappprototype;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.tectiv3.aes.RCTAesPackage;
import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;
import com.reactnativecommunity.geolocation.GeolocationPackage;
import io.realm.react.RealmReactPackage;
import com.mapbox.rctmgl.RCTMGLPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.igorbelyayev.rnlocalresource.RNLocalResourcePackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;

import com.viromedia.bridge.ReactViroPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            new RCTAesPackage(),
            new ReactNativePushNotificationPackage(),
            new GeolocationPackage(),
            new RealmReactPackage(),
            new RCTMGLPackage(),
            new AsyncStoragePackage(),
            new RNLocalResourcePackage(),
            new ReactViroPackage(ReactViroPackage.ViroPlatform.valueOf(BuildConfig.VR_PLATFORM))
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
