import React, { Component } from 'react';

import Routes from './js/Router'

export default class ArAppPrototype extends Component {
  render() {
    return <Routes />;
  }
}

module.exports = ArAppPrototype
