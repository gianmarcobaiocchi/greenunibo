<?php

/**
 * This class manages remade db.
 */
class DataBaseHelper{
    private $db;

    /**
     * Builder.
     */
    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if($this->db->connect_error){
            die("Connection failed: " . $this->db->connect_error);
        }
    }

    /**
     * Registers a new user in db.
     */
    public function registerUser($username, $password, $firstName, $secondName, $birthday) {
        $dt = date("Y-m-d", strtotime($birthday));
        $query = "INSERT INTO users(`username`, `password`, `first_name`, `second_name`, `birthday`, `score`)
                VALUES (?, ?, ?, ?, ?, 0)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssss', $username, $password, $firstName, $secondName, $dt);
        $stmt->execute();
    }

    /**
     * Gets specific user's data.
     */
    public function getUser($username) {
        $query = "SELECT username, first_name, second_name, birthday, score 
            FROM users WHERE username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**
     * Updates user's data.
     */
    public function modifyUserData($username, $password, $firstName, $secondName, $birthday) {
        $dt = date("Y-m-d", strtotime($birthday));
        if($password == null || $password == "" || $password === "null") {
            $query = "UPDATE users SET first_name = ?, second_name = ?, birthday = ? WHERE username = ?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('ssss', $firstName, $secondName, $dt, $username);
        } else {
            $query = "UPDATE users SET `password` = ?, first_name = ?, second_name = ?, birthday = ? WHERE username = ?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('sssss', $password, $firstName, $secondName, $dt, $username);
        }
        $stmt->execute();
    }

    /**
     * Checks if a user's credentials are right.
     */
    public function checkLogin($username, $password) {
        $query = "SELECT username, `password` FROM users WHERE username=? AND `password`=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();
        $users = $result->fetch_all(MYSQLI_ASSOC);
        return count($users) === 1;
    }

    /**
     * Returns the ranking position of a specific user.
     */
    public function getRankingPosition($username) {
        $query = "SELECT DISTINCT username FROM `users` WHERE score > (SELECT score FROM `users` WHERE username = ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        $users = $result->fetch_all(MYSQLI_ASSOC);
        return count($users) + 1;
    }

    /**
     * Adds points to the score of a user.
     */
    public function addPoints($username, $points) {
        $query = "UPDATE users SET score = ? WHERE username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $points, $username);
        $stmt->execute();
    }

    /**
     * Returns all locations.
     */
    public function getLocations() {
        $query = "SELECT l.*, g.id AS id_goal, g.name AS name_goal, g.icon AS icon_goal 
            FROM locations AS l JOIN goals AS g ON l.goal = g.id";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**
     * Gets visited goals of a user.
     */
    public function getVisitedGoals($username) {
        $query = "SELECT g.*
            FROM goals_visited AS gv JOIN goals AS g ON gv.goal = g.id 
            WHERE username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**
     * Adds a goal in the visited goals of a user.
     */
    public function addVisitedGoal($username, $goalId) {
        $query = "INSERT INTO goals_visited(`username`, `goal`)
            VALUES (?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("sd", $username, $goalId);
        $stmt->execute();
    }

    /**
     * Gets all markers' infos.
     */
    public function getInformations() {
        $query = "SELECT i.id AS `key`, i.infos AS infosFile, g.icon, g.model, q.data AS quiz
            FROM informations AS i JOIN goals AS g ON i.goal = g.id JOIN questions AS q ON i.quiz = q.id";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
}

$dbh = new DataBaseHelper("localhost", "root", "", "remake_db");
?>