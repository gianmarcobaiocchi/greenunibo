<?php
require("./databaseHelper.php");
/**
 * Reads the text file content.
 */
function getTextFromFile($filename) {
    $myfile = fopen($filename, "r") or die("Unable to open file!");
    $text = fread($myfile, filesize($filename));
    fclose($myfile);

    return $text;
}

if(!isset($res)) {
    $res = new stdClass();
}

if(isset($_GET["request"])) {
    if($_GET["request"] == "goalInfos") {
        $res->goals = array();
        $goalInfos = $dbh->getInformations();
        foreach($goalInfos as $g) {
            $tmp = new stdClass();
            $tmp->key = $g["key"];
            $tmp->infosFile = json_decode($g["infosFile"]);
            $tmp->icon = $g["icon"];
            $tmp->model = json_decode($g["model"]);
            $tmp->quiz = json_decode($g["quiz"]);
            array_push($res->goals, $tmp);
        }
        echo json_encode($res);
    }
}
?>