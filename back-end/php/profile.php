<?php
require("./databaseHelper.php");

if(!isset($res)) {
    $res = new stdClass();
}

//Login
if(isset($_GET["checkUserLogin"]) && $_GET["checkUserLogin"]) {
    if(isset($_POST["username"]) && isset($_POST["password"])) {
        $res->result = $dbh->checkLogin($_POST["username"], $_POST["password"]);
        echo json_encode($res);
    } else {
        $res->result = false;
        echo json_encode($res);
    }
}
//Registration 
else if(isset($_GET["registerUser"]) && $_GET["registerUser"]) {
    if(isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["firstName"]) && isset($_POST["secondName"]) && isset($_POST["birthday"])) {
        if(count($dbh->getUser($_POST["username"])) === 0) {
            $dbh->registerUser($_POST["username"], $_POST["password"], 
                $_POST["firstName"], $_POST["secondName"], $_POST["birthday"]);
            $res->result = true;
            echo json_encode($res);
        } else {
            $res->result = false;
            echo json_encode($res);
        }
    } else {
        $res->result = false;
        echo json_encode($res);
    }
}
//Get user data
else if(isset($_GET["getUserData"]) && $_GET["getUserData"]) {
    if(isset($_POST["username"])) {
        $res->user = $dbh->getUser($_POST["username"])[0];
        if($res !== null) {
            $res->rankingPosition = $dbh->getRankingPosition($_POST["username"]);
            $res->visitedGoals = $dbh->getVisitedGoals($_POST["username"]);
            echo json_encode($res);
        } else {
            $res->result = false;
            echo json_encode($res);
        }
    } else {
        $res->result = false;
        echo json_encode($res);
    }
}
//Modification user data 
else if(isset($_GET["editUserData"]) && $_GET["editUserData"]) {
    if(isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["firstName"]) && isset($_POST["secondName"]) && isset($_POST["birthday"])) {
        $dbh->modifyUserData($_POST["username"], $_POST["password"], 
        $_POST["firstName"], $_POST["secondName"], $_POST["birthday"]);
        $res->result = true;
        echo json_encode($res);
    } else {
        $res->result = false;
        echo json_encode($res);
    }
}
//Add points to user 
else if(isset($_GET["addPoints"]) && $_GET["addPoints"]) {
    if(isset($_POST["username"]) && isset($_POST["points"]) && isset($_POST["goalId"])) {
        $user = $dbh->getUser($_POST["username"])[0];
        $newScore = $user["score"] + $_POST["points"];
        $dbh->addPoints($_POST["username"], $newScore);
        $dbh->addVisitedGoal($_POST["username"], $_POST["goalId"]);
        $res->result = true;
        echo json_encode($res);
    } else {
        $res->result = false;
        echo json_encode($res);
    }
} else {
    $res->result = false;
    echo json_encode($res);
}

?>