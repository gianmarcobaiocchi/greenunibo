<?php
require("./databaseHelper.php");

if(!isset($res)) {
    $res = new stdClass();
}

$res->type = "FeatureCollection";
$res->features = array();

if(isset($_GET["request"]) && $_GET["request"] == "locations") {
    $locations = $dbh->getLocations();
    foreach($locations as $l) {
        $tmp1 = new stdClass();
        $tmp1->type = "Feature";
        $tmp1->id = $l["id"];
        //Properties
        $tmp2 = new stdClass();
        $tmp2->icon = "icon";
        $tmp2->place = new stdClass();
        $tmp2->place->name = $l["name"];
        $tmp2->place->description = $l["description"];
        $tmp2->place->image = $l["image"];
        $tmp2->goal = new stdClass();
        $tmp2->goal->name = $l["name_goal"];
        $tmp2->goal->icon = $l["icon_goal"];
        
        $tmp1->properties = $tmp2;
        $tmp1->geometry = new stdClass();
        $tmp1->geometry->type = "Point";
        $tmp1->geometry->coordinates = [$l["longitude"], $l["latitude"]];
        array_push($res->features, $tmp1);
    }
    echo json_encode($res);
}

?>