-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Set 08, 2020 alle 13:58
-- Versione del server: 10.4.13-MariaDB
-- Versione PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `remake_db`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `goals`
--

CREATE TABLE `goals` (
  `id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `model` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`model`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `goals`
--

INSERT INTO `goals` (`id`, `name`, `icon`, `model`) VALUES
(1, 'Goal 1: No Poverty', 'E-Goal-01.png', '{\"position\": [0, 0, 0], \"rotation\": [-90, 0, 90], \"scale\": [0.00025, 0.00025, 0.00025], \"source\": \"/res/assets/1_goal.vrx\", \"resources\": [\"/res/assets/1_goal_diffuse.png\"]}'),
(2, 'Goal 2: Zero Hunger', 'E-Goal-02.png', '{\"position\": [0, 0, 0], \"rotation\": [-90, 0, 90], \"scale\": [0.0005, 0.0005, 0.0005], \"source\": \"/res/assets/2_goal.vrx\", \"resources\": [\"/res/assets/2_goal_diffuse.png\"]}'),
(3, 'Goal 3: Good Health and Well-being', 'E-Goal-03.png', '{\"position\": [0, 0, 0], \"rotation\": [-90, 0, 0], \"scale\": [0.0001, 0.0001, 0.0001], \"source\": \"/res/assets/3_goal.vrx\", \"resources\": [\"/res/assets/2_goal_diffuse.png\"]}'),
(4, 'Goal 4: Quality Education', 'E-Goal-04.png', '{\"position\": [0, 0, 0], \"rotation\": [-90, 0, 90], \"scale\": [0.0005, 0.0005, 0.0005], \"source\": \"/res/assets/4_goal.vrx\", \"resources\": [\"/res/assets/2_goal_diffuse.png\"]}'),
(5, 'Goal 5: Gender Equality', 'E-Goal-05.png', '{\"position\": [0, 0, 0], \"rotation\": [-90, 0, 90], \"scale\": [0.00025, 0.00025, 0.00025], \"source\": \"/res/assets/5_goal.vrx\", \"resources\": [\"/res/assets/5_goal_diffuse.png\"]}'),
(6, 'Goal 6: Clean Water and Sanitation', 'E-Goal-06.png', '{\"position\": [0, 0, 0], \"rotation\": [-90, 0, 90], \"scale\": [0.00025, 0.00025, 0.00025], \"source\": \"/res/assets/6_goal.vrx\", \"resources\": [\"/res/assets/6_goal_diffuse.png\"]}'),
(7, 'Goal 7: Affordable and Clean Energy', 'E-Goal-07.png', '{\"position\": [0, 0, -0.01], \"rotation\": [-90, 0, 90], \"scale\": [0.00025, 0.00025, 0.00025], \"source\": \"/res/assets/7_goal.vrx\", \"resources\": [\"/res/assets/7_goal_diffuse.png\"]}');

-- --------------------------------------------------------

--
-- Struttura della tabella `goals_visited`
--

CREATE TABLE `goals_visited` (
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `goal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `goals_visited`
--

INSERT INTO `goals_visited` (`username`, `goal`) VALUES
('giammib', 1),
('giammib', 2),
('giammib', 3),
('giammib', 5),
('giammib', 6),
('giammib', 7),
('sully', 2),
('sully', 4);

-- --------------------------------------------------------

--
-- Struttura della tabella `informations`
--

CREATE TABLE `informations` (
  `id` int(11) NOT NULL,
  `quiz` int(11) NOT NULL,
  `goal` int(11) NOT NULL,
  `infos` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`infos`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `informations`
--

INSERT INTO `informations` (`id`, `quiz`, `goal`, `infos`) VALUES
(1, 1, 1, '{ \"infos\": [{\"image\": \"E-Goal-01.png\", \"width\": 0.1, \"height\": 0.1}, {\"text\": \"Even before the pandemic, the pace of global\\n\\r poverty reduction was decelerating,\\n\\r and it was projected that the global\\n\\r target of ending poverty by 2030 would be missed.\"}, {\"text\": \"The pandemic is pushing tens of millions\\n\\r of persons back into extreme poverty, undoing years of progress.\"}, {\"text\": \"While the pandemic has highlighted the need to strengthen\\n\\r social protection and emergency preparedness and response,\\n\\r those measures are insufficient to safeguard the poor\\n\\r and the vulnerable, who most need them.\"}, {\"text\": \"After a decline, from 15.7 per cent in 2010\\n\\r to 10.0 per cent in 2015, the pace of reduction\\n\\r of extreme poverty slowed further, with a nowcast\\n\\r rate of 8.2 per cent in 2019.\\n\\r The pandemic is reversing the trend of poverty reduction.\"}, {\"text\": \"According to the most recent estimates,\\n\\r the global extreme poverty rate is projected to be 8.4\\n\\r to 8.8 per cent in 2020, which is close to its level in 2017.\"}, {\"text\": \"Consequently, an estimated 40 to 60 million persons will\\n\\r be pushed back into extreme poverty, the first increase\\n\\r in global poverty in more than 20 years.\"}, {\"animatedImage\": \"E_GIF_01.gif\", \"width\": 0.1, \"height\": 0.1}]}'),
(2, 2, 2, '{ \"infos\": [{\"image\": \"E-Goal-02.png\", \"width\": 0.1, \"height\": 0.1}, {\"text\": \"Even before the pandemic, the pace of global\\n\\r poverty reduction was decelerating,\\n\\r and it was projected that the global\\n\\r target of ending poverty by 2030 would be missed.\"}, {\"text\": \"The pandemic is pushing tens of millions\\n\\r of persons back into extreme poverty, undoing years of progress.\"}, {\"text\": \"While the pandemic has highlighted the need to strengthen\\n\\r social protection and emergency preparedness and response,\\n\\r those measures are insufficient to safeguard the poor\\n\\r and the vulnerable, who most need them.\"}, {\"text\": \"After a decline, from 15.7 per cent in 2010\\n\\r to 10.0 per cent in 2015, the pace of reduction\\n\\r of extreme poverty slowed further, with a nowcast\\n\\r rate of 8.2 per cent in 2019.\\n\\r The pandemic is reversing the trend of poverty reduction.\"}, {\"text\": \"According to the most recent estimates,\\n\\r the global extreme poverty rate is projected to be 8.4\\n\\r to 8.8 per cent in 2020, which is close to its level in 2017.\"}, {\"text\": \"Consequently, an estimated 40 to 60 million persons will\\n\\r be pushed back into extreme poverty, the first increase\\n\\r in global poverty in more than 20 years.\"}, {\"animatedImage\": \"E_GIF_02.gif\", \"width\": 0.1, \"height\": 0.1}]}'),
(3, 3, 3, '{ \"infos\": [{\"image\": \"E-Goal-03.png\", \"width\": 0.1, \"height\": 0.1}, {\"text\": \"Even before the pandemic, the pace of global\\n\\r poverty reduction was decelerating,\\n\\r and it was projected that the global\\n\\r target of ending poverty by 2030 would be missed.\"}, {\"text\": \"The pandemic is pushing tens of millions\\n\\r of persons back into extreme poverty, undoing years of progress.\"}, {\"text\": \"While the pandemic has highlighted the need to strengthen\\n\\r social protection and emergency preparedness and response,\\n\\r those measures are insufficient to safeguard the poor\\n\\r and the vulnerable, who most need them.\"}, {\"text\": \"After a decline, from 15.7 per cent in 2010\\n\\r to 10.0 per cent in 2015, the pace of reduction\\n\\r of extreme poverty slowed further, with a nowcast\\n\\r rate of 8.2 per cent in 2019.\\n\\r The pandemic is reversing the trend of poverty reduction.\"}, {\"text\": \"According to the most recent estimates,\\n\\r the global extreme poverty rate is projected to be 8.4\\n\\r to 8.8 per cent in 2020, which is close to its level in 2017.\"}, {\"text\": \"Consequently, an estimated 40 to 60 million persons will\\n\\r be pushed back into extreme poverty, the first increase\\n\\r in global poverty in more than 20 years.\"}, {\"animatedImage\": \"E_GIF_03.gif\", \"width\": 0.1, \"height\": 0.1}]}'),
(4, 4, 4, '{ \"infos\": [{\"image\": \"E-Goal-04.png\", \"width\": 0.1, \"height\": 0.1}, {\"text\": \"Even before the pandemic, the pace of global\\n\\r poverty reduction was decelerating,\\n\\r and it was projected that the global\\n\\r target of ending poverty by 2030 would be missed.\"}, {\"text\": \"The pandemic is pushing tens of millions\\n\\r of persons back into extreme poverty, undoing years of progress.\"}, {\"text\": \"While the pandemic has highlighted the need to strengthen\\n\\r social protection and emergency preparedness and response,\\n\\r those measures are insufficient to safeguard the poor\\n\\r and the vulnerable, who most need them.\"}, {\"text\": \"After a decline, from 15.7 per cent in 2010\\n\\r to 10.0 per cent in 2015, the pace of reduction\\n\\r of extreme poverty slowed further, with a nowcast\\n\\r rate of 8.2 per cent in 2019.\\n\\r The pandemic is reversing the trend of poverty reduction.\"}, {\"text\": \"According to the most recent estimates,\\n\\r the global extreme poverty rate is projected to be 8.4\\n\\r to 8.8 per cent in 2020, which is close to its level in 2017.\"}, {\"text\": \"Consequently, an estimated 40 to 60 million persons will\\n\\r be pushed back into extreme poverty, the first increase\\n\\r in global poverty in more than 20 years.\"}, {\"animatedImage\": \"E_GIF_04.gif\", \"width\": 0.1, \"height\": 0.1}]}'),
(5, 5, 5, '{ \"infos\": [{\"image\": \"E-Goal-05.png\", \"width\": 0.1, \"height\": 0.1}, {\"text\": \"Even before the pandemic, the pace of global\\n\\r poverty reduction was decelerating,\\n\\r and it was projected that the global\\n\\r target of ending poverty by 2030 would be missed.\"}, {\"text\": \"The pandemic is pushing tens of millions\\n\\r of persons back into extreme poverty, undoing years of progress.\"}, {\"text\": \"While the pandemic has highlighted the need to strengthen\\n\\r social protection and emergency preparedness and response,\\n\\r those measures are insufficient to safeguard the poor\\n\\r and the vulnerable, who most need them.\"}, {\"text\": \"After a decline, from 15.7 per cent in 2010\\n\\r to 10.0 per cent in 2015, the pace of reduction\\n\\r of extreme poverty slowed further, with a nowcast\\n\\r rate of 8.2 per cent in 2019.\\n\\r The pandemic is reversing the trend of poverty reduction.\"}, {\"text\": \"According to the most recent estimates,\\n\\r the global extreme poverty rate is projected to be 8.4\\n\\r to 8.8 per cent in 2020, which is close to its level in 2017.\"}, {\"text\": \"Consequently, an estimated 40 to 60 million persons will\\n\\r be pushed back into extreme poverty, the first increase\\n\\r in global poverty in more than 20 years.\"}, {\"animatedImage\": \"E_GIF_05.gif\", \"width\": 0.1, \"height\": 0.1}]}'),
(6, 6, 6, '{ \"infos\": [{\"image\": \"E-Goal-06.png\", \"width\": 0.1, \"height\": 0.1}, {\"text\": \"Even before the pandemic, the pace of global\\n\\r poverty reduction was decelerating,\\n\\r and it was projected that the global\\n\\r target of ending poverty by 2030 would be missed.\"}, {\"text\": \"The pandemic is pushing tens of millions\\n\\r of persons back into extreme poverty, undoing years of progress.\"}, {\"text\": \"While the pandemic has highlighted the need to strengthen\\n\\r social protection and emergency preparedness and response,\\n\\r those measures are insufficient to safeguard the poor\\n\\r and the vulnerable, who most need them.\"}, {\"text\": \"After a decline, from 15.7 per cent in 2010\\n\\r to 10.0 per cent in 2015, the pace of reduction\\n\\r of extreme poverty slowed further, with a nowcast\\n\\r rate of 8.2 per cent in 2019.\\n\\r The pandemic is reversing the trend of poverty reduction.\"}, {\"text\": \"According to the most recent estimates,\\n\\r the global extreme poverty rate is projected to be 8.4\\n\\r to 8.8 per cent in 2020, which is close to its level in 2017.\"}, {\"text\": \"Consequently, an estimated 40 to 60 million persons will\\n\\r be pushed back into extreme poverty, the first increase\\n\\r in global poverty in more than 20 years.\"}, {\"animatedImage\": \"E_GIF_06.gif\", \"width\": 0.1, \"height\": 0.1}]}'),
(7, 7, 7, '{ \"infos\": [{\"image\": \"E-Goal-07.png\", \"width\": 0.1, \"height\": 0.1}, {\"text\": \"Even before the pandemic, the pace of global\\n\\r poverty reduction was decelerating,\\n\\r and it was projected that the global\\n\\r target of ending poverty by 2030 would be missed.\"}, {\"text\": \"The pandemic is pushing tens of millions\\n\\r of persons back into extreme poverty, undoing years of progress.\"}, {\"text\": \"While the pandemic has highlighted the need to strengthen\\n\\r social protection and emergency preparedness and response,\\n\\r those measures are insufficient to safeguard the poor\\n\\r and the vulnerable, who most need them.\"}, {\"text\": \"After a decline, from 15.7 per cent in 2010\\n\\r to 10.0 per cent in 2015, the pace of reduction\\n\\r of extreme poverty slowed further, with a nowcast\\n\\r rate of 8.2 per cent in 2019.\\n\\r The pandemic is reversing the trend of poverty reduction.\"}, {\"text\": \"According to the most recent estimates,\\n\\r the global extreme poverty rate is projected to be 8.4\\n\\r to 8.8 per cent in 2020, which is close to its level in 2017.\"}, {\"text\": \"Consequently, an estimated 40 to 60 million persons will\\n\\r be pushed back into extreme poverty, the first increase\\n\\r in global poverty in more than 20 years.\"}, {\"animatedImage\": \"E_GIF_07.gif\", \"width\": 0.1, \"height\": 0.1}]}');

-- --------------------------------------------------------

--
-- Struttura della tabella `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(800) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `goal` int(11) NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `locations`
--

INSERT INTO `locations` (`id`, `name`, `description`, `image`, `goal`, `longitude`, `latitude`) VALUES
(1, 'Location 1', 'DescrizioneRiccione è un comune italiano di 35 259 abitanti della provincia di Rimini, in Emilia-Romagna. Soprannominato la \"Perla Verde dell\'Adriatico\", è una nota località turistica estiva della Riviera romagnola. Dal 1990 può fregiarsi del titolo onorifico di città.', 'riccione.jpg', 1, 12.3, 44),
(2, 'Location 2', 'Parque La Carolina is a 165.5-acre (670,000 m²) park in the centre of the Quito central business district, bordered by the avenues Río Amazonas, de los Shyris, Naciones Unidas, Eloy Alfaro, and de la República.', 'parque-la-carolina.jpg', 2, 12, 43);

-- --------------------------------------------------------

--
-- Struttura della tabella `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`data`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `questions`
--

INSERT INTO `questions` (`id`, `data`) VALUES
(1, '{\r\n    \"question\": \"How many people didn\'t benefit from any form of social protection in 2016?\",\r\n    \"answers\": [\r\n        {\r\n            \"key\": 0,\r\n            \"text\": \"1 million\",\r\n            \"correct\": false\r\n        },\r\n        {\r\n            \"key\": 1,\r\n            \"text\": \"2.5 billion\",\r\n            \"correct\": false\r\n        },\r\n        {\r\n            \"key\": 2,\r\n            \"text\": \"4 billion\",\r\n            \"correct\": true\r\n        }\r\n    ]\r\n}'),
(2, '{\r\n    \"question\": \"How many percent of population is affected by moderate or sever food insicurity in 2018?\",\r\n    \"answers\": [\r\n        {\r\n            \"key\": 0,\r\n            \"text\": \"10%\",\r\n            \"correct\": false\r\n        },\r\n        {\r\n            \"key\": 1,\r\n            \"text\": \"26.4%\",\r\n            \"correct\": true\r\n        },\r\n        {\r\n            \"key\": 2,\r\n            \"text\": \"35.1%\",\r\n            \"correct\": false\r\n        }\r\n    ]\r\n}'),
(3, '{\r\n    \"question\": \"How many people in the world are covered by essential health services in 2017?\",\r\n    \"answers\": [\r\n        {\r\n            \"key\": 0,\r\n            \"text\": \"Less than half of the global population\",\r\n            \"correct\": true\r\n        },\r\n        {\r\n            \"key\": 1,\r\n            \"text\": \"A tenth of the global population\",\r\n            \"correct\": false\r\n        },\r\n        {\r\n            \"key\": 2,\r\n            \"text\": \"More than half of the global population\",\r\n            \"correct\": false\r\n        },\r\n        {\r\n            \"key\": 3,\r\n            \"text\": \"Three-quarters of the global population\",\r\n            \"correct\": false\r\n        }\r\n    ]\r\n}'),
(4, '{\r\n    \"question\": \"How many students remain out of reach for remote learning at least?\",\r\n    \"answers\": [\r\n        {\r\n            \"key\": 0,\r\n            \"text\": \"100 thousands\",\r\n            \"correct\": false\r\n        },\r\n        {\r\n            \"key\": 1,\r\n            \"text\": \"500 thousands\",\r\n            \"correct\": false\r\n        },\r\n        {\r\n            \"key\": 2,\r\n            \"text\": \"500 millions\",\r\n            \"correct\": true\r\n        },\r\n        {\r\n            \"key\": 3,\r\n            \"text\": \"1 billion\",\r\n            \"correct\": false\r\n        }\r\n    ]\r\n}'),
(5, '{\r\n    \"question\": \"How many are women accounted of health and social workers?\", \r\n    \"answers\": [\r\n        {\r\n            \"key\": 0,\r\n            \"text\": \"20%\",\r\n            \"correct\": false\r\n        },\r\n        {\r\n            \"key\": 1,\r\n            \"text\": \"70%\",\r\n            \"correct\": true\r\n        },\r\n        {\r\n            \"key\": 2,\r\n            \"text\": \"85%\",\r\n            \"correct\": false\r\n        }\r\n    ]\r\n}'),
(6, '{\r\n    \"question\": \"How many people worldwide lack basic handwashing facilities at home?\", \r\n    \"answers\": [\r\n        {\r\n            \"key\": 0,\r\n            \"text\": \"500 million\",\r\n            \"correct\": false\r\n        },\r\n        {\r\n            \"key\": 1,\r\n            \"text\": \"1 billion\",\r\n            \"correct\": false\r\n        },\r\n        {\r\n            \"key\": 2,\r\n            \"text\": \"3 billion\",\r\n            \"correct\": true\r\n        }\r\n    ]\r\n}'),
(7, '{\r\n    \"question\": \"How many people lacked electricity in 2018?\", \r\n    \"answers\": [\r\n        {\r\n            \"key\": 0,\r\n            \"text\": \"1 million\",\r\n            \"correct\": false\r\n        },\r\n        {\r\n            \"key\": 1,\r\n            \"text\": \"789 million\",\r\n            \"correct\": true\r\n        },\r\n        {\r\n            \"key\": 2,\r\n            \"text\": \"934 million\",\r\n            \"correct\": false\r\n        },\r\n        {\r\n            \"key\": 3,\r\n            \"text\": \"1 billion\",\r\n            \"correct\": false\r\n        }\r\n    ]\r\n}');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `first_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `second_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `score` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`username`, `password`, `first_name`, `second_name`, `birthday`, `score`) VALUES
('albye', '_J9..rasmH3Plmj8ucZM', 'Albert', 'Einstein', '2020-08-03', 22),
('giammib', 'oojxQadBDrZXz1HU53WGAg==', 'Gianmarco', 'Baiocchi', '1998-02-12', 65),
('jhonny', '_J9..rasmH3Plmj8ucZM', 'Jhonny', 'Bravo', '2020-04-06', 20),
('sully', 'oojxQadBDrZXz1HU53WGAg==', 'James', 'Sullivan', '2002-03-15', 15),
('tommy', '_J9..rasm.TV7il9LjPk', 'Tommaso', 'Paradiso', '1984-10-10', 10);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `goals`
--
ALTER TABLE `goals`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `goals_visited`
--
ALTER TABLE `goals_visited`
  ADD PRIMARY KEY (`username`,`goal`),
  ADD KEY `goals_key` (`goal`);

--
-- Indici per le tabelle `informations`
--
ALTER TABLE `informations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `goal` (`goal`),
  ADD KEY `questions_key` (`quiz`);

--
-- Indici per le tabelle `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `goal_key` (`goal`);

--
-- Indici per le tabelle `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `informations`
--
ALTER TABLE `informations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT per la tabella `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `goals_visited`
--
ALTER TABLE `goals_visited`
  ADD CONSTRAINT `goals_key` FOREIGN KEY (`goal`) REFERENCES `goals` (`id`),
  ADD CONSTRAINT `username_key` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Limiti per la tabella `informations`
--
ALTER TABLE `informations`
  ADD CONSTRAINT `goal_foreign_key` FOREIGN KEY (`goal`) REFERENCES `goals` (`id`),
  ADD CONSTRAINT `questions_key` FOREIGN KEY (`quiz`) REFERENCES `questions` (`id`);

--
-- Limiti per la tabella `locations`
--
ALTER TABLE `locations`
  ADD CONSTRAINT `goal_key` FOREIGN KEY (`goal`) REFERENCES `goals` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
